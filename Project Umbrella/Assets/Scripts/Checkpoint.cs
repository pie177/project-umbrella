﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Checkpoint : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponentInChildren<Transform>().CompareTag("Player_Sound"))
        {
            Save_And_Load.instance.xposition = transform.position.x;
            Save_And_Load.instance.yposition = transform.position.y;
            Save_And_Load.instance.zposition = transform.position.z;
            Save_And_Load.instance.sceneID = SceneManager.GetActiveScene().buildIndex;
            Save_And_Load.instance.roomNumber = other.GetComponentInParent<Character_Movement>().currentRoom;
            //Save_And_Load.instance.inventory = other.GetComponentInParent<PlayerInventory>().inventory;
            //Save_And_Load.instance.Item1Active = other.GetComponentInChildren<Iventory>().Item1UIActive;
            //Save_And_Load.instance.Item2Active = other.GetComponentInChildren<Iventory>().Item2UIActive;
            //Save_And_Load.instance.Item3Active = other.GetComponentInChildren<Iventory>().Item3UIActive;
            Save_And_Load.instance.Save();

            Destroy(this.gameObject);
        }
    }
}
