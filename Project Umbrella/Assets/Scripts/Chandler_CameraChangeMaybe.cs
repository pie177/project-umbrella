﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Chandler_CameraChangeMaybe : MonoBehaviour {

    [Tooltip("Use the array index of the RoomController CameraArray that you want the camera to swap to")]
    public int newRoomNum;
    [Tooltip("Use the array index of the RoomController CameraArray for the camera in this room")]
    public int oldRoomNum;

    //[SerializeField]
    //public GameObject[] cameraArray;
    [Tooltip("This is auto-found")]
    public GameObject roomController;

    [Tooltip("True: To the scene defined by the 'NextScene' variable")]
    public bool finalDoor = false;

    [Tooltip("Name of the next scene (only needs to be set if finalDoor = true)")]
    public string nextScene;

    Vector3 wallNormal;
    // Use this for initialization
    void Start () {
        roomController = GameObject.Find("RoomController");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (!finalDoor)
            {
                RaycastHit hit;
                if (Physics.Raycast(other.transform.position, other.transform.forward, out hit))
                {
                    wallNormal = -hit.normal;
                }
                other.GetComponentInParent<Character_Movement>().gameObject.transform.position = other.transform.position + wallNormal * 1.5f;
                other.GetComponentInParent<Character_Movement>().currentRoom = newRoomNum;
                if (roomController.GetComponent<Cameras>().cameraArray[newRoomNum] != null)
                {
                    roomController.GetComponent<Cameras>().cameraArray[newRoomNum].SetActive(!roomController.GetComponent<Cameras>().cameraArray[newRoomNum].activeSelf);
                    roomController.GetComponent<Cameras>().cameraArray[oldRoomNum].SetActive(!roomController.GetComponent<Cameras>().cameraArray[oldRoomNum].activeSelf);
                    int tempRoom = newRoomNum;
                    newRoomNum = oldRoomNum;
                    oldRoomNum = tempRoom;
                }
                if (Camera.main != null)
                {
                    other.transform.parent.GetComponent<Character_Movement>().m_Cam = Camera.main.transform;
                }
                else
                {
                    Debug.LogWarning(
                        "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
                    // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
                }
            }
            else
            {
                SceneManager.LoadScene(nextScene);
            }
        }
    }
}
