﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound_System : MonoBehaviour {

	public static Sound_System instance;

	public AudioSource backgroundMusic;
	public AudioSource playerTalking;
	public AudioSource playerGrunts;
    public AudioSource playerRepeatingSounds;
    public AudioSource backgroundSpeech;


    // Use this for initialization
    void Awake ()
	{
        
		if (instance == null)
		{
			instance = this;
		}

		if (instance != null && instance != this)
			Destroy(this.gameObject);

		DontDestroyOnLoad(instance);

    }

	void Start()
	{


        if (backgroundMusic != null && backgroundMusic.clip != null) 
		{
			backgroundMusic.Play ();
			Debug.Log ("Sound played!");
		}
	}

    public void PlayPlayerSpeech(AudioClip clipToPlay)
	{
		if (playerTalking != null && clipToPlay != null)
		{
			playerTalking.clip = clipToPlay;
			playerTalking.Play();
			Debug.Log ("Sound played!");
		}
	}

	public void PlayPlayerGrunts(AudioClip clipToPlay)
	{
		if (playerGrunts != null && clipToPlay != null)
		{
			playerGrunts.clip = clipToPlay;
			playerGrunts.Play();
			Debug.Log ("Sound played!");
		}
	}

	public void PlaySFX(AudioSource clipToPlay)
	{
		if (clipToPlay != null && clipToPlay.clip != null)
		{
			clipToPlay.Play ();
			Debug.Log ("Sound played!");
		}
	}

	public void PlayEnemySounds(AudioSource clipToPlay)
	{
		if (clipToPlay.clip != null && clipToPlay != null)
		{
			clipToPlay.Play();
			Debug.Log ("Sound played!");
		}
	}

    public void PlayRepeatingPlayerSounds(AudioClip clipToPlay)
    {
        if(clipToPlay != null)
        {
            playerRepeatingSounds.clip = clipToPlay;
            playerRepeatingSounds.Play();
            Debug.Log("Repeating Sound played");
        }
    }

    public void PlayBackgroundSpeech(AudioClip clipToPlay)
    {
        if(clipToPlay != null)
        {
            backgroundSpeech.clip = clipToPlay;
            backgroundSpeech.Play();
        }
    }
    
    public void StopBackgroundSpeech()
    {
        backgroundSpeech.Stop();
    }
}
