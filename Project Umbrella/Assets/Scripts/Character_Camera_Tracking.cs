﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character_Camera_Tracking : MonoBehaviour {

	Camera assignedCamera;
	Collider playerCollider;
	Plane[] frustrum;



	// Use this for initialization
	void Start () 
	{
		assignedCamera = gameObject.GetComponent<Camera> ();
		playerCollider = GameObject.FindGameObjectWithTag ("Player").GetComponent<Collider> ();
		frustrum = GeometryUtility.CalculateFrustumPlanes (assignedCamera);
		if (tag == "MainCamera") 
		{
			assignedCamera.enabled = true;
		} 
		else 
		{
			assignedCamera.enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{

		if (GeometryUtility.TestPlanesAABB (frustrum, playerCollider.bounds)) 
		{
			Ray cameraRay = new Ray (transform.position, playerCollider.transform.position - transform.position);
			RaycastHit hit;
			//Debug.Log ("Can see player!");
			if (Physics.Raycast (cameraRay.origin, cameraRay.direction, out hit)) 
			{
				Debug.DrawRay (transform.position, playerCollider.transform.position - transform.position);
				//Debug.Log ("I just raycasted!");
				if (hit.collider.gameObject.tag == "Player") 
				{
                    
                        //Debug.Log (playerCollider.gameObject.name + " found in " + assignedCamera.name);
                        assignedCamera.tag = "MainCamera";
                        assignedCamera.enabled = true;
                   
				} 
				else 
				{
                        assignedCamera.tag = "Camera";
                        assignedCamera.enabled = false;
                    
				}
			}

		} 
		else
		{
			assignedCamera.tag = "Camera";
			assignedCamera.enabled = false;
		}
	}
}
