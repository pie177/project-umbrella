﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public GameObject mainMenu;
    public GameObject optionsMenu;

    public AudioSource buttonSound;

    public string creditsScene;
    public string firstLevel;
    // Use this for initialization
    void Start () {
       mainMenu.SetActive(true);
       optionsMenu.SetActive(false);

        buttonSound = GameObject.Find("ButtonSound").GetComponent<AudioSource>();
        
    }
	
	// Update is called once per frame
	void Update () {

    }

    public void _PlayButton()
    {
        buttonSound.Play();
        Save_And_Load.instance.start = true;
        SceneManager.LoadScene("_LoadingScene");
        Time.timeScale = 1;
    }

    public void _LoadButton()
    {
        buttonSound.Play();
        GameObject.Find("SaveAndLoad").GetComponent<Save_And_Load>().Load();
        //call the load button script here to keep it organized with the other menu buttons
    }

    public void optionsButton()
    {
        buttonSound.Play();
        optionsMenu.SetActive(true);
        GameObject.Find("Back Button").GetComponent<Button>().Select();
        mainMenu.SetActive(false);
    }

    public void creditsButton()
    {
        buttonSound.Play();
        SceneManager.LoadScene(creditsScene);
    }

    public void exitButton() {
        buttonSound.Play();
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
    }

    public void _ExitButton()
    {
        buttonSound.Play();
        Application.Quit();
    }
}
