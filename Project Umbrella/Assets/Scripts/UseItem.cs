﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UseItem : MonoBehaviour
{

    public Image Item1UIActive;
    public Image Item2UIActive;
    public Image Item3UIActive;

    public GameObject InteractPrompt;

    string interactableItem;

    public GameObject[] interactables;

    public Character_Movement charMove;

    // Use this for initialization
    void Start()
    {
        interactables = GameObject.FindGameObjectsWithTag("Interactable");
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnTriggerStay(Collider other)
    {

        if (other.gameObject.CompareTag("Interactable"))
        {

            interactableItem = other.name;
            {
                switch (interactableItem)
                {
                    case "interact1":
                        if(Item1UIActive.enabled == true)
                        {
                            InteractPrompt.SetActive(true);
                            if (Input.GetAxis("Interact") > 0)
                            {
                                charMove.charAnimator.SetTrigger("isPickup");
                                Item1UIActive.enabled = false;
                                InteractPrompt.SetActive(false);
                                Destroy(other.gameObject);
                            }
                        }
                        break;
                    case "interact2":
                        if (Item2UIActive.enabled == true)
                        {
                            InteractPrompt.SetActive(true);
                            if (Input.GetAxis("Interact") > 0)
                            {
                                charMove.charAnimator.SetTrigger("isPickup");
                                Item2UIActive.enabled = false;
                                InteractPrompt.SetActive(false);
                                Destroy(other.gameObject);
                            }
                        }
                        break;
                    case "interact3":
                        if (Item3UIActive.enabled == true)
                        {
                            InteractPrompt.SetActive(true);
                            if (Input.GetAxis("Interact") > 0)
                            {
                                charMove.charAnimator.SetTrigger("isPickup");
                                Item3UIActive.enabled = false;
                                InteractPrompt.SetActive(false);
                                Destroy(other.gameObject);
                            }
                        }
                        break;
                }
            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        InteractPrompt.SetActive(false);
    }

}
