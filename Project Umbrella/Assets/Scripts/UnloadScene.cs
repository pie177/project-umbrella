﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UnloadScene : MonoBehaviour {

    public GameObject player;
    public GameObject roomControl;
    int roomNumber;

	// Use this for initialization
	void Start ()
    {
        if (Save_And_Load.instance.loaded == true)
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponentInParent<Character_Movement>().gameObject;
            roomControl = GameObject.Find("RoomController");
            player.transform.position = new Vector3(Save_And_Load.instance.xposition, Save_And_Load.instance.yposition, Save_And_Load.instance.zposition);
            player.GetComponentInParent<Character_Movement>().currentRoom = Save_And_Load.instance.roomNumber;
            //player.GetComponentInParent<PlayerInventory>().inventory = Save_And_Load.instance.inventory;
            roomNumber = Save_And_Load.instance.roomNumber;
            foreach (GameObject myCamera in roomControl.GetComponent<Cameras>().cameraArray)
            {
                myCamera.SetActive(false);
            }

            roomControl.GetComponent<Cameras>().cameraArray[roomNumber].SetActive(true);

            player.GetComponentInParent<Character_Movement>().m_Cam = roomControl.GetComponent<Cameras>().cameraArray[roomNumber].transform;
        }

        SceneManager.UnloadScene("_LoadingScene");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
