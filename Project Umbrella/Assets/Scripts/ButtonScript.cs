﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : MonoBehaviour {

    public bool isHit;
    public bool inRange;
    bool buttonHeld;

    public Material hit, notHit;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        hitButton();
        if (isHit)
        {
            GetComponent<Renderer>().material.color = hit.color;
        }
        else
        {
            GetComponent<Renderer>().material.color = notHit.color;
        }
	}

    public void hitButton()
    {
        if (inRange)
        {
            if (Input.GetAxis("Interact") > 0 && !buttonHeld)
            {
                GetComponentInParent<PuzzleController>().interactionPrompt.SetActive(false);
                buttonHeld = true;
                isHit = true;
                GetComponentInParent<PuzzleController>().buttonPuzzle(System.Array.IndexOf(GetComponentInParent<PuzzleController>().buttonArray, this.gameObject));
            }
        }
        if(Input.GetAxis("Interact") == 0)
        {
            buttonHeld = false;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GetComponentInParent<PuzzleController>().interactionPrompt.SetActive(true);
            inRange = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GetComponentInParent<PuzzleController>().interactionPrompt.SetActive(true);
            inRange = false;
        }
    }
}
