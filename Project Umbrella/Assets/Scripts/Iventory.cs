﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Iventory : MonoBehaviour {

    public Image Item1UIActive;
    public Image Item2UIActive;
    public Image Item3UIActive;

    public GameObject InteractPrompt;

    string itemName;

    public GameObject[] Items;

    public Character_Movement charMove;
	// Use this for initialization
	void Start () {

    Item1UIActive.enabled = false;
    Item2UIActive.enabled = false;
    Item3UIActive.enabled = false;

     Items = GameObject.FindGameObjectsWithTag("puzzle Item");
    

}

// Update is called once per frame
void Update () {
    }

    public void OnTriggerStay (Collider other)
    {

        if (other.gameObject.CompareTag("puzzle Item"))
        {
            //InteractPrompt.SetActive(true);
            itemName = other.name;
            Debug.Log(itemName);
            if (Input.GetAxis("Interact") > 0)
            {
                charMove.charAnimator.SetTrigger("isPickup");
                switch (itemName)
                {
                    case "Key":
                        Item1UIActive.enabled = true;
                        break;
                    case "item2":
                        Item2UIActive.enabled = true;
                        break;
                    case "item3":
                        Item3UIActive.enabled = true;
                        break;
                }
                //Destroy(other.gameObject);
                //InteractPrompt.SetActive(false);
            }     
        }      
    }
    public void OnTriggerExit(Collider other)
    {
        //InteractPrompt.SetActive(false);
    }

}
