﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character_Flashlight : MonoBehaviour {

    public Light flashLight;
    bool buttonHeld;

	// Use this for initialization
	void Start ()
    {
        flashLight.enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if((Input.GetAxis("Flashlight") == -1 || Input.GetKeyDown(KeyCode.F)) && !buttonHeld)
        {
            flashLight.enabled = !flashLight.enabled;
            buttonHeld = true;
        }
        if(Input.GetAxis("Flashlight") == 0)
        {
            buttonHeld = false;
        }
	}
}
