﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
    // Transform of the camera to shake. Grabs the gameObject's transform
    // if null.
    public Transform camTransform;

    // How long the object should shake for.
    public float shakeDuration = 0f;

    // Amplitude of the shake. A larger value shakes the camera harder.
    public float shakeAmount = 0.7f;
    public float decreaseFactor = 1.0f;

    Vector3 originalPos;

    void Awake()
    {
        if (camTransform == null)
        {
            camTransform = GetComponent(typeof(Transform)) as Transform;
        }

       
    }

    void OnEnable()
    {
        originalPos = camTransform.localPosition;
    }
    void start()
    {
        shakeChange();
    }

    void Update()
    {
        if (shakeDuration > 0)
        {
            if(shakeAmount > 0.01f)
            {
                shakeAmount = shakeAmount - 0.005f;
            }
            else if (shakeAmount <= 0.01f)
            {
                shakeAmount = 0.01f;
            }
            camTransform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;

            shakeDuration -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shakeDuration = 5f;
            camTransform.localPosition = originalPos;
        }
    }

    IEnumerator shakeChange()
    {
        yield return new WaitForSeconds(5);
        shakeAmount = 0.01f;
    }
}