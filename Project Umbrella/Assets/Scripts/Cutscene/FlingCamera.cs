﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlingCamera : MonoBehaviour {

    public float timeToFly;
    public float throwSpeed;
    public float fallTime;

    public Rigidbody cameraRB;

    public GameObject Nothing;
    public GameObject RECcanvas;
    public GameObject liam;
	// Use this for initialization
	void Start () {
        cameraRB = GetComponent<Rigidbody>();
        
	}

    private void Update()
    {
        transform.LookAt(liam.transform);
        throwCamera();
    }

    public void throwCamera()
    {
        cameraRB.GetComponent<Rigidbody>().useGravity = true;
        cameraRB.AddForce(0, 0, throwSpeed, ForceMode.Impulse);
        StartCoroutine("falling");
    }

    public IEnumerator falling()
    {
        yield return new WaitForSeconds(fallTime);
        RECcanvas.SetActive(false);
        Nothing.SetActive(true);
    }

}
