﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatCamera : MonoBehaviour {

    public float horizontalFloatSpeed;
    public float verticalFloatSpeed;
    public float amountofFloatyness;

    public GameObject lightning;


    Quaternion floatyCameraPos;
	// Use this for initialization
	void Start () {
        
        floatyCameraPos = this.transform.localRotation;
	}
	
	// Update is called once per frame
	void Update () {
        floatyCameraPos.x += Mathf.Sin(Time.realtimeSinceStartup * horizontalFloatSpeed) * amountofFloatyness;
        floatyCameraPos.y += Mathf.Sin(Time.realtimeSinceStartup * verticalFloatSpeed) * amountofFloatyness;
        this.transform.localRotation = floatyCameraPos;   
	}
}
