﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayDialogue : MonoBehaviour {
    public AudioSource LiamIntro1;
    public AudioSource LiamIntro2;
    public AudioSource Pilot;
    public AudioSource CameraMan;

    public GameObject Liam;

    Animator liamAnimation;
    // Use this for initialization
    void Start () {
        liamAnimation = Liam.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
    }

    public void CallLiamIntroOne()
    {
        bool hasPlayed = false;

        if (hasPlayed == false)
        {
            StartCoroutine(LiamIntroOne());
            hasPlayed = true;
        }
    }

    public void callCameraGuy()
    {
        bool hasPlayed = false;

        if(hasPlayed == false)
        {
            StartCoroutine(cameraGuy());
            hasPlayed = true;
        }
    }

    public IEnumerator LiamIntroOne()
    {
        this.GetComponent<PlayDialogue>().LiamIntro1.Play();
        yield return new WaitForSeconds(LiamIntro1.clip.length);
        liamAnimation.SetTrigger("Lightning");
    }

    public IEnumerator cameraGuy()
    {
        this.GetComponent<PlayDialogue>().CameraMan.Play();
        yield return new WaitForSeconds(CameraMan.clip.length);
        liamAnimation.SetTrigger("Talking");
    }
}
