﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrashCopter : MonoBehaviour {

    public GameObject lightning;
	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        spinHelicopter();
    }

    void spinHelicopter()
    {
        if(lightning.activeInHierarchy == true)
        {
            this.gameObject.transform.Rotate(Vector3.up * 200 * Time.deltaTime);
        }
    }
}
