﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningStrike : MonoBehaviour
{

    public GameObject camera;
    public GameObject pilot;
    public GameObject liam;
    GameObject pilotDialogue;
    public Animator lightningStruck;

    int wobbleStateHash = Animator.StringToHash("Base Layer.LightningHit");
    public float cameraTurnSpeed = 5;
    // Use this for initialization
    void Start()
    {
        lightningStruck.SetTrigger(0);
        
    }

    public void StatCall()
    {
        

        camera.GetComponent<CameraShake>().enabled = true;

        StartCoroutine(turnCamera());
        lightningStruck.GetComponent<Animator>();
        lightningStruck.SetTrigger(wobbleStateHash);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator turnCamera()
    {


        //camera.GetComponent<FloatCamera>().enabled = false;
        Transform target = pilot.transform;
        //float timeOver = 0.0f;
        //float timeToTurn = 5f;
        yield return new WaitForSeconds(5);
        bool turnOne = true;
        while (turnOne)
        {
            //timeOver += Time.deltaTime;
            Quaternion originalRotation = camera.transform.rotation;
            Vector3 dirFromTarget = target.position - camera.transform.position;
            Vector3 newDirection = Vector3.RotateTowards(camera.transform.forward, dirFromTarget, Time.deltaTime * cameraTurnSpeed, 0);
            camera.transform.rotation = Quaternion.LookRotation(newDirection);
            if(originalRotation!=camera.transform.rotation)
            {
                yield return new WaitForEndOfFrame();
            }
            else
            {
                turnOne = false;
            }
        }

        pilotDialogue = GameObject.Find("Pilot");
        pilotDialogue.GetComponent<AudioClip>();
        pilotDialogue.GetComponent<AudioSource>().Play();

        yield return new WaitForSeconds(5);
        bool turnTwo = true;
        while (turnTwo)
        {
            target = liam.transform;
            //timeOver += Time.deltaTime;
            Quaternion originalRotation = camera.transform.rotation;
            Vector3 dirFromTarget = target.position - camera.transform.position;
            Vector3 newDirection = Vector3.RotateTowards(camera.transform.forward, dirFromTarget, Time.deltaTime * cameraTurnSpeed, 0);
            camera.transform.rotation = Quaternion.LookRotation(newDirection);
            if (originalRotation != camera.transform.rotation)
            {
                yield return new WaitForEndOfFrame();
            }
            else
            {
                turnTwo = false;
            }
        }

        if(turnTwo == false)
        {
            camera.GetComponent<FlingCamera>().enabled = true;
            pilotDialogue = GameObject.Find("WilhelmScream");
            pilotDialogue.GetComponent<AudioClip>();
            pilotDialogue.GetComponent<AudioSource>().Play();
        }
    }
}
