﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum puzzleType{
	buttonPuzzle,
	itemPuzzle
};

public enum itemType{
	key,
	crowbar
};

public class PuzzleController : MonoBehaviour {

	public puzzleType puzType = puzzleType.buttonPuzzle;
	public itemType itType = itemType.key;
	public GameObject interactionPrompt;
	public GameObject[] buttonArray;
	public PlayerInventory playerInv;
	public bool inRange = false;
    public GameObject mykeyModel;
    public GameObject myCrowModel;
    public GameObject doorToOpen;
    public Animator charAnim;

    private void Awake()
    {

    }

    // Use this for initialization
    void Start () {
        if (puzType == puzzleType.itemPuzzle && itType == itemType.key)
        {
            myCrowModel.SetActive(false);
            mykeyModel.SetActive(true);
        }
        else if (puzType == puzzleType.itemPuzzle && itType == itemType.crowbar)
        {
            mykeyModel.SetActive(false);
            myCrowModel.SetActive(true);
        }
    }
	
	// Update is called once per frame
	void Update () {
		if (puzType == puzzleType.itemPuzzle) {
			itemPuzzle ();
		}
	}

	public void buttonPuzzle(int myIndex){
        int totalHit = 0;
        for(int i = 0; i < myIndex; i++)
        {
            if (!buttonArray[i].GetComponent<ButtonScript>().isHit)
            {
                foreach(GameObject button in buttonArray)
                {
                    button.GetComponent<ButtonScript>().isHit = false;
                }
                return;
            }
        }
        foreach(GameObject button in buttonArray)
        {
            if (button.GetComponent<ButtonScript>().isHit)
            {
                totalHit++;
            }
        }
        if(totalHit >= buttonArray.Length)
        {
            doorToOpen.SetActive(false);
        }
	}

	public void itemPuzzle(){
		if (inRange) {
			if (Input.GetAxis ("Interact") > 0) {
                charAnim.SetTrigger("isPickup");
				if (itType == itemType.key) {
					playerInv.AddKey ();
				}
				if (itType == itemType.crowbar) {
					playerInv.AddCrow ();
				}
				interactionPrompt.SetActive (false);
				this.gameObject.SetActive (false);
			}
		}
	}

	public void OnTriggerEnter(Collider other){
		if(other.CompareTag("Player")){
            charAnim = other.GetComponentInChildren<Animator>();
			interactionPrompt.SetActive (true);
			inRange = true;
		}
	}

	public void OnTriggerExit(Collider other){
		if(other.CompareTag("Player")){
			interactionPrompt.SetActive (false);
			inRange = false;
		}
	}
}
