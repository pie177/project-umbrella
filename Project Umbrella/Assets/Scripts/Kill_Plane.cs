﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class Kill_Plane : MonoBehaviour {

    public string sceneToLoad;

    public float timeTillLoad;
    public float currentTime;

    bool loadNext = false;
    public bool startCount = false;

    public GameObject staticControl;
    public VideoPlayer staticEffect;

    public void Start()
    {
        staticControl = GameObject.FindGameObjectWithTag("CameraSelector");
        staticEffect = staticControl.GetComponentInChildren<VideoPlayer>();
    }
    public void Update()
    {
        if(currentTime <= 0 && startCount)
        {
            currentTime = 0;
            loadNext = true;
        }
        if (startCount)
        {
            currentTime -= 1 * Time.deltaTime;
            staticEffect.targetCameraAlpha = -1 + (1 / (currentTime / timeTillLoad));
            Debug.Log(staticEffect.targetCameraAlpha);
        }
        if (loadNext)
        {
            SceneManager.LoadScene(sceneToLoad);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            staticControl.GetComponent<enemyInRange>().enabled = false;
            currentTime = timeTillLoad;
            startCount = true;
        }
    }
}
