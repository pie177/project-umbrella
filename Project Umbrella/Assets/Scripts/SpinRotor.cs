﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinRotor : MonoBehaviour {

    public float speed = 100;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(this.gameObject.name == "v_personnel_transport_rotor")
        {
            topRotor();
        }
        if(this.gameObject.name == "v_personnel_transport_backrotor")
        {
            backRotor();
        }

	}

    public void topRotor()
    {
        transform.Rotate(Vector3.forward * speed * Time.deltaTime);
    }

    public void backRotor()
    {
        transform.Rotate(Vector3.right * speed * Time.deltaTime);
    }
}
