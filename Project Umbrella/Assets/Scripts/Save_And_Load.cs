﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class Save_And_Load : MonoBehaviour {

    public static Save_And_Load instance;

    void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }

        if (instance != null && instance != this)
            Destroy(this.gameObject);

        DontDestroyOnLoad(instance);

    }

    public float xposition;
    public float yposition;
    public float zposition;
    public int enemyCount;
    public int sceneID;
    public int roomNumber;

    public bool loaded = false;
    public bool start = true;
    //public List<GameObject> inventory;
    //public bool Item1Active;
    //public bool Item2Active;
    //public bool Item3Active;

    public void Save()
    {
        StartCoroutine(YelloDot());

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop) + "/saveInfo.dat");

        SaveData data = new SaveData();
        data.xposition = xposition;
        data.yposition = yposition;
        data.zposition = zposition;
        data.enemyCount = enemyCount;
        data.sceneID = sceneID;
        data.roomNumber = roomNumber;
        //data.inventory = inventory;
        //data.Item1Active = Item1Active;
        //data.Item2Active = Item2Active;
        //data.Item3Active = Item3Active;

        //Debug.Log(data.inventory.Count);
        bf.Serialize(file, data);
        file.Close();

        Debug.Log("Saved!");
    }

    public void Load()
    {
        if(File.Exists(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop) + "/saveInfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop) + "/saveInfo.dat", FileMode.Open);
            SaveData data = (SaveData)bf.Deserialize(file);


            xposition = data.xposition;
            yposition = data.yposition;
            zposition = data.zposition;
            enemyCount = data.enemyCount;
            sceneID = data.sceneID;
            roomNumber = data.roomNumber;
            //inventory = data.inventory;
            //Item1Active = data.Item1Active;
            //Item2Active = data.Item2Active;
            //Item3Active = data.Item3Active;
            file.Close();
            loaded = true;
            start = false;
            SceneManager.LoadScene("_LoadingScene");

            Vector3 position = new Vector3(xposition, yposition, zposition);
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            //player.transform.position = position;
        }
    }

    IEnumerator YelloDot()
    {
        GameObject Timestamp = GameObject.Find("Time Stamp");
        //Timestamp.GetComponent<Camera_Time_Stamp>().SavingIcon();
        Timestamp.GetComponent<Camera_Time_Stamp>().SavingIcon();

        yield return new WaitForSeconds(3f);

        Timestamp.GetComponent<Camera_Time_Stamp>().NoLongerSaving();
    }
}

[Serializable]
class SaveData
{
    public float xposition;
    public float yposition;
    public float zposition;
    public int enemyCount;
    public int sceneID;
    public int roomNumber;
    //public List<GameObject> inventory;
    //public bool Item1Active;
    //public bool Item2Active;
    //public bool Item3Active;
}
