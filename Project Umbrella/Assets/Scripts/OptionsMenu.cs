﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OptionsMenu : MonoBehaviour {
    public static OptionsMenu instance;

    public GameObject soundController;
    public GameObject mainMenu;
    public GameObject optionsMenu;
    public GameObject pauseMenu;

    public Slider masterSlider;
    public Slider musicSlider;
    public Slider SFXSlider;

    public Toggle buttonSoundToggle;

    public AudioSource backgroundMusic;
    public AudioSource playerTalking;
    public AudioSource playerGrunts;
    public AudioSource buttonSound;
    public AudioSource playerRepeatingSounds;
    public AudioSource backgroundSpeech;

    float audioLevelMaster;
    float audioLevelBGM;
    float audioLevelSFX;

    public int toggleState;

    //public float masterSend;
    //public float BGMSend;
    //public float SFXSend;

    public void Awake()
    {
        masterSlider.value = 0.5f;
        musicSlider.value = 0.5f;
        SFXSlider.value = 0.5f;

        toggleState = PlayerPrefs.GetInt("ToggleState");
        if(toggleState == 1)
        {
            buttonSoundToggle.isOn = true;
        }
        else if(toggleState == 0)
        {
            buttonSoundToggle.isOn = false;
        }
    }
    // Use this for initialization
    void Start () {
        backgroundMusic = GameObject.Find("BackgroundSound").GetComponent<AudioSource>();
        playerTalking = GameObject.Find("PlayerSpeech").GetComponent<AudioSource>();
        playerGrunts = GameObject.Find("PlayerGrunts").GetComponent<AudioSource>();
        buttonSound = GameObject.Find("ButtonSound").GetComponent<AudioSource>();
        playerRepeatingSounds = GameObject.Find("PlayerRepeatingSounds").GetComponent<AudioSource>();
        backgroundSpeech = GameObject.Find("BackgroundSpeech").GetComponent<AudioSource>();

        masterSlider.value = 0.5f;
        musicSlider.value = 0.5f;
        SFXSlider.value = 0.5f;
        buttonSoundToggle.isOn = PlayerPrefs.GetInt("ToggleState") == 1;

        audioLevelMaster = PlayerPrefs.GetFloat("MasterVolume");
        audioLevelBGM = PlayerPrefs.GetFloat("BGMVolume");
        audioLevelSFX = PlayerPrefs.GetFloat("SFXVolume");
        toggleState = PlayerPrefs.GetInt("ToggleState");

        if (audioLevelMaster != 0.5 || audioLevelBGM != 0.5 || audioLevelSFX != 0.5)
        {
            masterSlider.value = audioLevelMaster;
            musicSlider.value = audioLevelBGM;
            SFXSlider.value = audioLevelSFX;

            AudioListener.volume = audioLevelMaster;

            backgroundMusic.volume = audioLevelBGM;

            playerTalking.volume = audioLevelSFX;
            playerGrunts.volume = audioLevelSFX;
        }
        else
        {
            masterSlider.value = 0.5f;
            musicSlider.value = 0.5f;
            SFXSlider.value = 0.5f;
        }
        if (soundController == null)
        {
            soundController = GameObject.FindGameObjectWithTag("Sound_System");
        }

    }
	
	// Update is called once per frame
	void Update () {

        if (buttonSoundToggle.isOn == true)
        {
            toggleState = 1;
            buttonSound.volume = 0;

        }
        else if (buttonSoundToggle.isOn == false)
        {
            toggleState = 0;
            buttonSound.volume = 1;
        }
    }

    public void masterVolumeController()
    {
        AudioListener.volume = masterSlider.value;
        //masterSend = masterSlider.value;
    }

    public void musicVolumeController()
    {
        if(backgroundMusic != null)
        backgroundMusic.volume = musicSlider.value;
        //BGMSend = musicSlider.value;
    }

    public void SFXVolumeController()
    {
        if (playerTalking != null && playerGrunts != null)
        {
            playerTalking.volume = SFXSlider.value;
            playerGrunts.volume = SFXSlider.value;
            playerRepeatingSounds.volume = SFXSlider.value;
            backgroundSpeech.volume = SFXSlider.value;
        }
        //SFXSend = SFXSlider.value;
    }
    public void backButtonMainMenu()
    {
        buttonSound.Play();
        if (SceneManager.GetActiveScene().name == "_MainMenuJeffreyLuther")
        {
            mainMenu.SetActive(true);
            GameObject.Find("StartButton").GetComponent<Button>().Select();
            optionsMenu.SetActive(false);
        }
        else
        {
            backButtonPauseMenu();
        }
    }

    public void backButtonPauseMenu()
    {
        buttonSound.Play();
        pauseMenu.SetActive(true);
        GameObject.Find("ResumeButton").GetComponent<Button>().Select();
        optionsMenu.SetActive(false);
    }

    public void OnDestroy()
    {
        PlayerPrefs.SetFloat("MasterVolume", masterSlider.value);
        PlayerPrefs.SetFloat("BGMVolume", musicSlider.value);
        PlayerPrefs.SetFloat("SFXVolume", SFXSlider.value);
        PlayerPrefs.SetInt("ToggleState", toggleState);
    }
}
