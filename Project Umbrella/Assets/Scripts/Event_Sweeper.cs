﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event_Sweeper : MonoBehaviour {

    [Tooltip("This is all the items that should be cleared when the trigger is struck")]
    public GameObject[] itemsToDestroy;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            foreach(GameObject item in itemsToDestroy)
            {
                Destroy(item);
            }
        }
    }
}
