﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle_Button_Combination : Puzzle_Item {

    public Puzzle_Button[] buttons;

	// Use this for initialization
	void Start ()
    {
        interactPrompt.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            if(buttons[i].collected)
            {
                if(buttons[i].previousButtonInSequence >= 0)
                {
                    if(buttons[buttons[i].previousButtonInSequence].collected == false)
                    {
                        foreach(Puzzle_Button button in buttons)
                        {
                            button.collected = false;
                        }
                        collected = false;
                        break;
                    }
                    else
                    {
                        collected = true;
                    }
                }
            }
        }
	}
}
