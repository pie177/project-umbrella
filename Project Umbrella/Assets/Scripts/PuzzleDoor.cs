﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum doorType{
	key,
	crowbar
};

public class PuzzleDoor : MonoBehaviour {

	public doorType dType = doorType.key;

	public PlayerInventory playerInv;

	public GameObject interactionPrompt;

	public bool inRange = false;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (inRange) {
			if (Input.GetAxis ("Interact") > 0) {
				if (dType == doorType.key) {
					if(playerInv.RemoveKey())
                    {
                        interactionPrompt.SetActive(false);
                        this.gameObject.SetActive(false);
                    }
				}
				if (dType == doorType.crowbar) {
					if(playerInv.RemoveCrow())
                    {
                        interactionPrompt.SetActive(false);
                        this.gameObject.SetActive(false);
                    }
				}
			}
		}
	}

	public void OnTriggerEnter(Collider other){
		if(other.CompareTag("Player")){
			interactionPrompt.SetActive (true);
			inRange = true;
		}
	}

	public void OnTriggerExit(Collider other){
		if(other.CompareTag("Player")){
			interactionPrompt.SetActive (false);
			inRange = false;
		}
	}
}
