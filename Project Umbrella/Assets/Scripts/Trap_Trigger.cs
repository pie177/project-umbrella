﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap_Trigger : MonoBehaviour {

    public GameObject trap;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        { 
            if (trap != null)
            {
                trap.GetComponent<Trap_Info>().isTriggered = true;
            }

            Destroy(this.gameObject);
        }
        else
        {
            return;
        }
    }
}
