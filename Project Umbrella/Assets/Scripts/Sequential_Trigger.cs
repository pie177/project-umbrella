﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sequential_Trigger : MonoBehaviour {

    public GameObject triggerToSetActive;
    [Tooltip("Do not use both previous interaction AND previous trigger, one OR the other.")]
    public GameObject previousInteractionInSequence;
    [Tooltip("Do not use both previous interaction AND previous trigger, one OR the other.")]
    public GameObject previousTriggerInSequence;
    bool usingTrigger;

	// Use this for initialization
	void Awake ()
    {
        if (previousTriggerInSequence == null)
            usingTrigger = false;
        else
            usingTrigger = true;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (previousInteractionInSequence != null)
        {
            if (!previousInteractionInSequence.activeInHierarchy && triggerToSetActive != null && !usingTrigger)
            {
                triggerToSetActive.SetActive(true);
            }
        }
        if(previousTriggerInSequence == null && triggerToSetActive != null && usingTrigger)
        {
            triggerToSetActive.SetActive(true);
        }
	}
}
