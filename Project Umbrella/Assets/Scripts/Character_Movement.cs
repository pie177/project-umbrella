using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Character_Movement : MonoBehaviour
{
    public AudioClip heavyBreathing;
	public AudioClip sighOfRelief;
	private Sound_System soundSystem;
    private bool isMoving = false;
    [Tooltip("Do not touch")]
    public bool climbing;
    bool crouch = false;
    bool crouchHeld;
    bool isRunning = false;
    [HideInInspector]
    public bool isPickup;
    float timer;
    private bool runningPlayed;

    [Tooltip("Do not touch")]
    public Transform m_Cam;                  // A reference to the main camera in the scenes transform
    private Vector3 m_CamForward;             // The current forward direction of the camera
    public Vector3 m_Move;
    [Tooltip("The speed the player walks in m/s")]
	public float walkSpeed;
    public float climbSpeed;
    [Tooltip("Time spent running before character starts breathing heavily")]
    public float timeTillTired;
    [Tooltip("The walkspeed is multiplied by this value when running")]
    public float runMultiplier;
    [Tooltip("The walkspeed is multiplied by this value when crawling")]
    public float crawlMultiplier;
    [Tooltip("Do not touch")]
    public int currentRoom = 0;
    [Tooltip("Do not touch")]
    SphereCollider soundCollider;
    [Tooltip("The size (based on transform scale) that the sound collider is when walking")]
	public float walkingSoundScale;
    [Tooltip("The size (based on transform scale) that the sound collider is when running")]
    public float runningSoundScale;
    [Tooltip("The size (based on transform scale) that the sound collider is when crawling")]
    public float crawlingSoundScale;
    [Tooltip("The default size (based on transform scale) of the sound collider")]
    private float soundScale;
	private float currentSoundScale;
    [Tooltip("Size of the player when crawling (Base height is 2.0")]
	public float crouchHeight = 1.5f;
	private float standingHeight = 2.0f;
    [Tooltip("Do not touch")]
    private CapsuleCollider playerCollider;
    [Tooltip("Do not touch")]
    public Vector3 startClimb;
    [Tooltip("Do not touch")]
    public Vector3 endClimb;
    [Tooltip("Do not touch")]
    public Character_Climbing cClimb;
    [Tooltip("Do not touch")]
    public GameObject charModel;
    [Tooltip("Do not touch")]
    public Vector3 climbingNormal;

    public Animator charAnimator;

    public bool isFalling;
    public bool landing;

	//[SerializeField]
	//public GameObject[] cameraArray = new GameObject[1];
    
    private void Start()
    {
        //charAnimator = GetComponentInChildren<Animator>();
        // get the transform of the main camera
        if (Camera.main != null)
        {
            m_Cam = Camera.main.transform;
        }
        else
        {
            Debug.LogWarning(
                "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
            // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
        } 
		soundCollider = gameObject.GetComponent<SphereCollider> ();
		playerCollider = gameObject.transform.GetComponentInChildren<CapsuleCollider> ();
		soundSystem = GameObject.FindGameObjectWithTag ("Sound_System").GetComponent<Sound_System>();
    }


    private void Update()
    {
        if(m_Cam == null)
        {
            m_Cam = Camera.main.transform;
        }
        if (isFalling)
        {
            charAnimator.SetBool("falling", true);
            if (Physics.Raycast(transform.position, Vector3.down, 1f))
            {

                charAnimator.SetBool("falling", false);
                isFalling = false;
                charAnimator.SetTrigger("landing");
            }
        }
        if (!Physics.Raycast(transform.position, Vector3.down, 2f))
        {
            if (!climbing)
            {
                isFalling = true;
            }
        }
        CharacterMovementKeys ();
		// read inputs
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");
        if (Input.GetAxis("Crouch") > 0 && !crouchHeld)
        {
            crouchHeld = true;
            crouch = !crouch;
        }
        if(Input.GetAxis("Crouch") == 0)
        {
            crouchHeld = false;
        }
        if (isRunning && isMoving && soundSystem != null)
        {
            timer += Time.deltaTime;
            if (timer >= timeTillTired && heavyBreathing != null && runningPlayed == false)
            {
                Debug.Log("Character tired");
                soundSystem.PlayRepeatingPlayerSounds(heavyBreathing);
                runningPlayed = true;
                timer = 0;
            }
        }
        else if (!isRunning && runningPlayed)
        {
            runningPlayed = false;
            if (soundSystem != null && soundSystem.playerRepeatingSounds.clip == heavyBreathing)
            {
                soundSystem.playerRepeatingSounds.Stop();
                soundSystem.PlayPlayerGrunts(sighOfRelief);
            }
            timer = 0;
            Debug.Log("Sound stopped");
        }
        if(!isMoving && runningPlayed)
        {
            runningPlayed = false;
            if (soundSystem != null && soundSystem.playerRepeatingSounds.clip == heavyBreathing)
            {
                soundSystem.playerRepeatingSounds.Stop();
                soundSystem.PlayPlayerGrunts(sighOfRelief);
            }
            timer = 0;
            Debug.Log("Sound stopped");
        }

		//Adjustment by Michael for Character_Camera_Tracking
		//m_Cam = Camera.main.transform;

		// calculate move direction to pass to character
		if (m_Cam != null)
		{
			// calculate camera relative direction to move:
			if (!isMoving)
			{
				m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
			}
            Vector3 newVector;

            m_Move = v * m_CamForward + h * (newVector = Quaternion.AngleAxis(90, Vector3.up) * m_CamForward);
		}
        AnimationUpdate();
    }

	private void CharacterMovementKeys()
    {
        if (!isPickup)
        {
            if (!isFalling)
            {
                if (!climbing)
                {
                    if (!landing)
                    {
                        GetComponent<Rigidbody>().useGravity = true;
                        if (isRunning = Input.GetAxis("Sprint") > 0)
                        {
                            m_Move *= runMultiplier;
                            currentSoundScale = runningSoundScale;
                        }

                        else
                        {
                            currentSoundScale = walkingSoundScale;
                        }

                        if (crouch)
                        {
                            m_Move *= crawlMultiplier;
                            currentSoundScale = crawlingSoundScale;
                            playerCollider.height = crouchHeight;
                            playerCollider.center = new Vector3(0, -.3f, 0);
                        }

                        else
                        {
                            playerCollider.height = standingHeight;
                            playerCollider.center = Vector3.zero;
                        }

                        if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0 || Mathf.Abs(Input.GetAxis("Vertical")) > 0)
                        {
                            isMoving = true;
                            transform.Translate(m_Move * walkSpeed * Time.deltaTime);
                            soundScale = currentSoundScale;
                            var newRotation = Quaternion.LookRotation(m_Move).eulerAngles;
                            charModel.transform.rotation = Quaternion.Slerp(charModel.transform.rotation, Quaternion.Euler(newRotation), 30 * Time.deltaTime);
                        }
                        else
                        {
                            isMoving = false;
                            soundScale = 1;
                        }
                        soundCollider.radius = soundScale;
                    }
                }

                else if (climbing)
                {
                    if (soundSystem != null && runningPlayed == true)
                    {
                        soundSystem.playerRepeatingSounds.Stop();
                        soundSystem.PlayPlayerGrunts(sighOfRelief);
                        runningPlayed = false;
                        timer = 0;
                        Debug.Log("Grunts Stopped");
                    }
                    GetComponent<Rigidbody>().useGravity = false;
                    var newRotation = Quaternion.LookRotation(climbingNormal * -1).eulerAngles;
                    charModel.transform.rotation = Quaternion.Slerp(charModel.transform.rotation, Quaternion.Euler(newRotation), 30 * Time.deltaTime);


                    if (transform.position.y >= endClimb.y - 1.3f)
                    {
                        charAnimator.SetBool("endClimbing", true);
                    }
                    if (transform.position.y < endClimb.y + 1.5f)
                    {
                        transform.Translate(Vector3.up * climbSpeed * Time.deltaTime);
                    }
                    else
                    {
                        transform.Translate(charModel.transform.forward * 50 * Time.deltaTime);
                        climbing = false;
                        cClimb.climging = false;
                        charAnimator.SetBool("endClimbing", false);
                    }

                    //if (transform.position.y > endClimb.y - 1.5f)
                    //{
                    //    charAnimator.SetBool("endClimbing", true);
                    //    charAnimator.gameObject.transform.Translate(Vector3.up * -climbSpeed * crawlMultiplier * Time.deltaTime);
                    //}
                    //if (transform.position.y > endClimb.y + 1)
                    //{
                    //    transform.Translate(climbingNormal * -1 * 30 * Time.deltaTime);
                    //    charAnimator.gameObject.transform.position = new Vector3(transform.position.x, transform.position.y - 1, transform.position.z);
                    //    climbing = false;
                    //    cClimb.climging = false;
                    //    charAnimator.SetBool("endClimbing", false);
                    //}
                    //else if (transform.position.y < endClimb.y + 1)
                    //{
                    //    transform.Translate(Vector3.up * climbSpeed * crawlMultiplier * Time.deltaTime);
                    //}
                }
            }
            else
            {
                transform.Translate(charModel.transform.forward * 2f * Time.deltaTime);
            }
        }
	}

    public void AnimationUpdate()
    {
        if (isMoving)
        {
            charAnimator.SetBool("isWalk", true);
        }
        else
        {
            charAnimator.SetBool("isWalk", false);
        }
        if (isRunning)
        {
            charAnimator.SetBool("isRun", true);
        }
        else
        {
            charAnimator.SetBool("isRun", false);
        }
        if (climbing)
        {
            charAnimator.SetBool("isClimbing", true);
        }
        else
        {
            charAnimator.SetBool("isClimbing", false);
        }
        if (crouch)
        {
            charAnimator.SetBool("isCrouch", true);
        }
        else
        {
            charAnimator.SetBool("isCrouch", false);
        }
    }

	//public void OnTriggerEnter(Collider other){
	//	if (other.CompareTag ("RoomTrigger")) {
	//		if (cameraArray[currentRoom + 1] != null) {
	//			cameraArray [currentRoom].SetActive (!cameraArray [currentRoom].activeSelf);
	//			cameraArray [currentRoom + 1].SetActive (!cameraArray [currentRoom + 1].activeSelf);
	//			currentRoom++;
	//		}
	//		if (Camera.main != null)
	//		{
	//			m_Cam = Camera.main.transform;
	//		}
	//		else
	//		{
	//			Debug.LogWarning(
	//				"Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
	//			// we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
	//		} 
	//	}
	//}
}


