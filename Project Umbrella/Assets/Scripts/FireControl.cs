﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireControl : MonoBehaviour {

    public GameObject[] fires;
    public GameObject sprinkler;
    public GameObject door;

    bool notRan = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(door.activeInHierarchy != true && notRan)
        {
            TurnOnSprinklers();
            notRan = false;
        }
	}

    public void TurnOnSprinklers()
    {
        sprinkler.GetComponent<ParticleSystem>().Play();

        foreach (GameObject fire in fires)
        {
            ParticleSystem ps = fire.GetComponent<ParticleSystem>();

            var main = ps.main;

            main.loop = false;
        }
    }
}
