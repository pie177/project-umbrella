﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Trap_Info : MonoBehaviour {

    [Tooltip("The scene that is loaded when the trap is activated")]
    public string sceneToLoad = "_MainMenu";
    [Tooltip("True: trap is activated from the start, False: (Default) trap is not active until triggered")]
    public bool isTriggered;
    
    bool playerDead;
    [Tooltip("How long after the player is hit until the sceneToLoad is loaded")]
    public float timeTillDeath = 2;
    float timer;
    [Tooltip("Do not touch")]
    public Animator animator;
    [SerializeField]
    [Tooltip("Do not touch")]
    string animationParameterName;

	// Use this for initialization
	void Start ()
    {
        animator = GetComponentInParent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (isTriggered && playerDead)
        {
            timer += Time.deltaTime;
            if (timer >= timeTillDeath)
            {
                SceneManager.LoadScene(sceneToLoad);
            }
        }

        if(isTriggered)
        {
            animator.SetBool(animationParameterName, true);
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if(isTriggered)
        {
            if(other.CompareTag("Player"))
            {
                playerDead = true;
                Rigidbody character = GameObject.Find("Character").GetComponent<Rigidbody>();
                character.freezeRotation = false;
                character.AddExplosionForce(500, transform.position, 6);
                animator.SetBool(animationParameterName, false);
            }
        }
    }
}
