﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour {

    public RectTransform [] credits;
    public float creditsScrollSpeed;
    public string mainMenu;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 updatePos = new Vector3(0, creditsScrollSpeed, 0);
        foreach (RectTransform text in credits )
        {
            text.localPosition += updatePos;
        }

	}

    public void MenuButton()
    {
        SceneManager.LoadScene(mainMenu);
    }
}
