﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadScreen : MonoBehaviour {

    public GameObject loadingScreen;
    public Slider slider;
    

	// Use this for initialization
	void Start ()
    {
        StartCoroutine(LoadScene());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(2.5f);

        AsyncOperation myLoad;
        if(Save_And_Load.instance.start == true)
        {
            myLoad = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
            Save_And_Load.instance.start = false;
        }

        else if (Save_And_Load.instance.loaded == true)
        {
            myLoad = SceneManager.LoadSceneAsync(Save_And_Load.instance.sceneID, LoadSceneMode.Additive);
        }
        else
        {
            myLoad = SceneManager.LoadSceneAsync(Save_And_Load.instance.sceneID + 1, LoadSceneMode.Additive);
        }

        loadingScreen.SetActive(true);

        while(!myLoad.isDone)
        {
            float progress = Mathf.Clamp01(myLoad.progress);

            slider.value = progress;

            yield return null;
        }
    }
}
