﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Camera_Time_Stamp : MonoBehaviour {

    public Text timeStamp;
    [Tooltip("Measured in whole hours, 24 hour clock format")]
    public int startingHour;
    [Tooltip("Measured in whole minutes")]
    public int startingMinute;
    [Tooltip("Measured in whole seconds")]
    public int startingSecond;

    public GameObject recDot;
    public GameObject saveImage;

    float secondGained;

    static Camera_Time_Stamp instance;

    //private void Awake()
    //{
    //    if (instance == null)
    //    {
    //        instance = this;
    //    }
    //    if(instance != this)
    //    {
    //        Destroy(this.gameObject);
    //    }

    //    DontDestroyOnLoad(instance);
    //}

    // Use this for initialization
    void Start ()
    {
        saveImage.SetActive(false);
    }
	
	// Update is called once per frame
	void Update ()
    {
        secondGained += Time.deltaTime;
        if(secondGained >=1)
        {
            secondGained = 0;
            startingSecond++;
        }
        if(startingSecond >= 60)
        {
            startingMinute++;
            startingSecond = 0;
        }

        if(startingMinute >= 60)
        {
            startingHour++;
            startingMinute = 0;
        }

        if(startingHour > 23)
        {
            startingHour = 0;
            startingMinute = 0;
            startingSecond = 0;
        }

        timeStamp.text = startingHour.ToString("00") + ":" + startingMinute.ToString("00") + ":" + startingSecond.ToString("00");
	}

    public void SavingIcon()
    {
        recDot.SetActive(false);
        saveImage.SetActive(true);
    }

    public void NoLongerSaving()
    {
        recDot.SetActive(true);
        saveImage.SetActive(false);
    }
}
