﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class enemyInRange : MonoBehaviour {

    public VideoPlayer staticPlayer;
    public float baseStaticLevel = .06f;
    public float staticThicknessMax;
    public float distanceThreshold = 10;
    public Sight_AI_Control[] controlDistances;
    public float closestDistance = 11;
    public float closingDistance;
    public float alphaPercentage;
    float inverseThreshold;

	// Use this for initialization
	void Start ()
    {
        staticPlayer.targetCameraAlpha = baseStaticLevel;
        inverseThreshold = 1 / distanceThreshold;
	}
	
	// Update is called once per frame
	void Update ()
    {
        closestDistance = 11;
		foreach(Sight_AI_Control control in controlDistances)
        {
            if(control.distance <= distanceThreshold && control != null)
            {
                if(control.distance < closestDistance)
                {
                    closestDistance = control.distance;
                }
            }
        }

        if (closestDistance >= distanceThreshold)
            staticPlayer.targetCameraAlpha = baseStaticLevel;
        else
        {
            closingDistance = distanceThreshold - closestDistance;
            alphaPercentage = closingDistance * inverseThreshold;
            if(alphaPercentage > staticThicknessMax)
            {
                staticPlayer.targetCameraAlpha = staticThicknessMax;
            }
            else
                staticPlayer.targetCameraAlpha = alphaPercentage;
        }
	}
}
