﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dot_Flicker : MonoBehaviour {

    public Image imageToFlicker;
    public float howOftenDotFlickers = 0.5f;
    float time;

	// Use this for initialization
	void Start ()
    {
        imageToFlicker = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        time += Time.deltaTime;
        if(time >= howOftenDotFlickers)
        {
            time = 0;
            imageToFlicker.enabled = !imageToFlicker.enabled;
        }
	}
}
