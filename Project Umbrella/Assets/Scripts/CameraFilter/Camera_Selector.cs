﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Camera_Selector : MonoBehaviour {

    [SerializeField]
    public VideoPlayer videoPlayer;
    public Camera[] camerasInScene;

	// Use this for initialization
	void Start ()
    {
        videoPlayer.isLooping = true;
	}
	
	// Update is called once per frame
	void Update ()
    {
		foreach(Camera currentCamera in camerasInScene)
        {
            if(currentCamera.gameObject.activeInHierarchy == true)
            {
                videoPlayer.targetCamera = currentCamera;
            }
            else
            {
                continue;
            }
        }
	}
}
