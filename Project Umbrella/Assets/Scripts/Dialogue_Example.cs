﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogue_Example : MonoBehaviour {

	private Sound_System soundSystem;
	public AudioClip playerCommunication;

	void Start()
	{
		soundSystem = GameObject.FindGameObjectWithTag ("Sound_System").GetComponent<Sound_System>();
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Player")) 
		{
			soundSystem.PlayPlayerSpeech (playerCommunication);
			Destroy (this.gameObject);
		}
	}
}
