﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInventory : MonoBehaviour {

	public List<GameObject> inventory;

	public List<GameObject> uiInv;

	public Canvas inventoryUI;

	public GameObject crowbar;
	public GameObject key;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void AddKey(){
		inventory.Add (key);
		UIUpdate ();
	}

	public void AddCrow(){
		inventory.Add (crowbar);
		UIUpdate ();
	}

	public bool RemoveKey(){
        foreach (GameObject item in inventory)
        {
            if (item.CompareTag("KeyUI"))
            {
                inventory.Remove(key);
                UIUpdate();
                return true;
            }
        }
        UIUpdate();
        return false;
    }

	public bool RemoveCrow(){
        foreach (GameObject item in inventory)
        {
            if (item.CompareTag("CrowUI"))
            {
                inventory.Remove(crowbar);
                UIUpdate();
                return true;
            }
        }
		UIUpdate ();
        return false;
	}

	public void UIUpdate(){
		foreach (GameObject item in uiInv) {
			Destroy (item);
		}
		uiInv = new List<GameObject>();
		foreach (GameObject item in inventory) {
			uiInv.Add(Instantiate (item, inventoryUI.transform));
			//item.GetComponent<RectTransform> ().anchoredPosition3D = new Vector3 (-25f + (-50 * inventory.IndexOf(item)), 25, 0);
		}
		foreach (GameObject item in uiInv) {
			item.GetComponent<RectTransform> ().anchoredPosition3D = new Vector3 (-25f + (-50 * uiInv.IndexOf(item)), 25, 0);
		}
	}
}
