﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character_Climbing : MonoBehaviour {

    [Tooltip("Set to the InteractionPrompt UI item in the scene")]
	public GameObject InteractPrompt;
	GameObject climbable;
	float startTime;
	float journeyLength;
	Vector3 startPos;
	Vector3 endPos;
    [Tooltip("Autoset")]
	public Character_Movement charMovement;
    [Tooltip("Do not touch")]
	public bool climging;
    [Tooltip("Do not touch")]
    public Vector3 climbNormal;
    bool buttonHeld;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Climbable"))
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit))
            {
                climbNormal = hit.normal;
                charMovement.climbingNormal = climbNormal;
            }
        }
    }

    public void OnTriggerStay(Collider other)
	{

		if (other.gameObject.CompareTag ("Climbable")) {
			InteractPrompt.SetActive (true);
			if (Input.GetAxis ("Interact") > 0 && !climging && !buttonHeld) {
				climbable = other.gameObject;
				startPos = gameObject.transform.parent.transform.parent.transform.position;
				endPos = new Vector3(startPos.x, other.GetComponent<Renderer>().bounds.max.y, startPos.z);
				charMovement.startClimb = startPos;
				charMovement.endClimb = endPos;
				climging = true;
                buttonHeld = true;
				charMovement.climbing = true;
			}
            if(Input.GetAxis("Interact") == 0)
            {
                buttonHeld = false;
            }
		}
	}

	public void OnTriggerExit(Collider other)
	{
		InteractPrompt.SetActive(false);
	}

//	public void StartClimbing()
//	{
//		while ((climbable.GetComponent<Renderer> ().bounds.max.y + 1) > gameObject.transform.position.y) 
//		{
//			float distCovered = (Time.time - startTime) * 1.0f;
//			float fracJourney = distCovered / journeyLength;
//			gameObject.transform.parent.transform.parent.transform.position = Vector3.Lerp (startPos, endPos, fracJourney);
//			//gameObject.transform.parent.transform.parent.transform.Translate (new Vector3(0, 1, 0) * Time.deltaTime);
//			//transform.position = new Vector3 (transform.position.x, transform.position.y + 1, transform.position.z);
//		}
//	}
}
