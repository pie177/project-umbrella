﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle_Button : Puzzle_Item {

    [SerializeField]
    public Material buttonActiveColor;
    [SerializeField]
    public Material buttonInactiveColor;
    public int previousButtonInSequence;

	// Use this for initialization
	void Start ()
    {
        interactPrompt.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (withinRange)
        {
            if (Input.GetAxis("Interact") > 0)
            {
                PuzzleActive();
                interactPrompt.SetActive(false);
                withinRange = false;
                collected = true;
            }
        }

        if(!collected)
        {
            PuzzleInactive();
        }

    }

    public override void PuzzleActive()
    {
        GetComponent<Renderer>().material.color = buttonActiveColor.color;
        searchingCollider.enabled = false;
    }

    public override void PuzzleInactive()
    {
        GetComponent<Renderer>().material = buttonInactiveColor;
        searchingCollider.enabled = true;
        collected = false;
    }
}
