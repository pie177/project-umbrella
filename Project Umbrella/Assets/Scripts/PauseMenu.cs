﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public GameObject pauseMenu;
    public GameObject optionsMenu;

    public AudioSource buttonSound;
    bool buttonHeld;
    // Use this for initialization
    void Start () {
        buttonSound = GameObject.Find("ButtonSound").GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;
        if ((Input.GetAxis("Cancel") > 0 && pauseMenu.activeInHierarchy == false && optionsMenu.activeInHierarchy == false && sceneName != "_MainMenuJeffreyLuther") && !buttonHeld)
        {
            pauseMenu.SetActive(true);
            buttonHeld = true;
            Time.timeScale = 0;
        }
        else if ((Input.GetAxis("Cancel") > 0 && pauseMenu.activeInHierarchy == true || Input.GetAxis("Cancel") > 0 && optionsMenu.activeInHierarchy == true && sceneName != "_MainMenuJeffreyLuther") && !buttonHeld)
        {
            optionsMenu.SetActive(false);
            buttonHeld = true;
            pauseMenu.SetActive(false);
            Time.timeScale = 1;
        }
        else if(pauseMenu.activeInHierarchy == false && optionsMenu.activeInHierarchy == false)
        {
            Time.timeScale = 1;
        }
        if(Input.GetAxis("Cancel") == 0)
        {
            buttonHeld = false;
        }
	}

    public void _MainMenuButton()
    {
        buttonSound.Play();
        SceneManager.LoadScene(0);
    }

    public void _OptionsButton()
    {
        buttonSound.Play();
        optionsMenu.SetActive(true);
        GameObject.Find("Back Button").GetComponent<Button>().Select();
        pauseMenu.SetActive(false);
    }

    public void _ResumeButton()
    {
        buttonSound.Play();
        optionsMenu.SetActive(false);
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
    }
}
