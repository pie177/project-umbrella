﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puzzle : MonoBehaviour {

    public Puzzle_Item[] keys;
    public GameObject interactPrompt;
    public int numKeysCollected;
    public bool isReadyToUnlock;

    string itemName;
    public Image Item1UIActive;
    public Image Item2UIActive;
    public Image Item3UIActive;

    // Use this for initialization
    void Start () {
        Item1UIActive = GameObject.Find("Image (1)").GetComponent<Image>();
        Item2UIActive = GameObject.Find("Image (3)").GetComponent<Image>();
        Item3UIActive = GameObject.Find("Image (5)").GetComponent<Image>();
    }
	
	// Update is called once per frame
	void Update ()
    {
		if(isReadyToUnlock)
        {
            if(Input.GetAxis("Interact") > 0)
            {
                foreach (Puzzle_Item key in keys)
                {
                    itemName = key.tag;
                    if (Input.GetAxis("Interact") > 0)
                    {
                        switch (itemName)
                        {
                            case "slot1Item":
                                Item1UIActive.enabled = false;
                                break;
                            case "slot2Item":
                                Item2UIActive.enabled = false;
                                break;
                            case "slot3Item":
                                Item3UIActive.enabled = false;
                                break;
                        }
                    }
                    interactPrompt.SetActive(false);
                    Destroy(this.gameObject);
                }
            }
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            foreach (Puzzle_Item key in keys)
            {
                if (key.collected)
                {
                    numKeysCollected++;
                }
            }

            if (numKeysCollected >= keys.Length)
            {
                interactPrompt.SetActive(true);
                isReadyToUnlock = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            foreach (Puzzle_Item key in keys)
            {
                if (key.collected)
                {
                    numKeysCollected--;
                }
            }

            interactPrompt.SetActive(false);
            isReadyToUnlock = false;
        }
    }
}
