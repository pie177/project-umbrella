﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vent_ChangeRoom : MonoBehaviour {
    [Tooltip("Use the array index of the RoomController VentArray that you want it to go to")]
	public int newRoomVent;
    [Tooltip("Use the array index of the RoomController VentArray that is THIS vent")]
	public int oldRoomVent;

    [Tooltip("Use the array index of the RoomController CameraArray that you want the camera to swap to")]
    public int newRoomNum;
    [Tooltip("Use the array index of the RoomController CameraArray for the camera in this room")]
    public int oldRoomNum;
	//[SerializeField]
	//public GameObject[] cameraArray;
    [Tooltip("This is auto-found")]
	public GameObject roomController;
    [Tooltip("Should be set to the Telepoint of the THIS vent")]
	public GameObject spawnPoint;

	// Use this for initialization
	void Start () {
		roomController = GameObject.Find("RoomController");
	}

	// Update is called once per frame
	void Update () {

	}

	public void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			if (roomController.GetComponent<Cameras>().cameraArray[newRoomNum] != null)
			{
				roomController.GetComponent<Cameras>().cameraArray[newRoomNum].SetActive(!roomController.GetComponent<Cameras>().cameraArray[newRoomNum].activeSelf);
				roomController.GetComponent<Cameras>().cameraArray[oldRoomNum].SetActive(!roomController.GetComponent<Cameras>().cameraArray[oldRoomNum].activeSelf);
				int tempRoom = newRoomNum;
				newRoomNum = oldRoomNum;
				oldRoomNum = tempRoom;
			}
			if (Camera.main != null)
			{
				other.transform.parent.GetComponent<Character_Movement> ().m_Cam = Camera.main.transform;
			}
			else
			{
				Debug.LogWarning(
					"Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
				// we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
			}
			if (roomController.GetComponent<Cameras>().ventArray[newRoomVent] != null)
			{
				other.transform.GetComponentInParent<Character_Movement>().gameObject.transform.position = roomController.GetComponent<Cameras> ().ventArray [newRoomVent].GetComponentInChildren<Vent_ChangeRoom>().spawnPoint.transform.position;
				
			}
			else
			{
				Debug.LogWarning("Warning: no new vent found.");
			}
		}
	}
}
