﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpenClose : MonoBehaviour {

    public GameObject doorToUse;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (doorToUse.active == true)
            {
                doorToUse.SetActive(false);
            }
            else
            {
                doorToUse.SetActive(true);
            }
        }
    }
}
