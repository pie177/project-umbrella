﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puzzle_Item : MonoBehaviour
{

    public GameObject interactPrompt;
    [SerializeField]
    public Collider searchingCollider;
    public bool withinRange = false;
    public bool collected = false;

    string itemName;
    public Sprite customImage;
    public Image Item1UIActive;
    public Image Item2UIActive;
    public Image Item3UIActive;

    bool slot1Full;
    bool slot2Full;
    bool slot3Full;

    Character_Movement playerChar;

    // Use this for initialization
    void Start()
    {
        Item1UIActive = GameObject.Find("Image (1)").GetComponent<Image>();
        Item2UIActive = GameObject.Find("Image (3)").GetComponent<Image>();
        Item3UIActive = GameObject.Find("Image (5)").GetComponent<Image>();
        //Item1UIActive = GameObject.Find("Image (1)").GetComponent<Image>();
        Item1UIActive.enabled = false;
        Item2UIActive.enabled = false;
        Item3UIActive.enabled = false;
        interactPrompt.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (withinRange)
        {
            itemName = this.tag;
            //Debug.Log(itemName);
            if (Input.GetAxis("Interact") > 0)
            {
                switch (itemName)
                {
                    case "slot1Item":
                        if(Item1UIActive.enabled == false)
                        {
                            playerChar.charAnimator.SetTrigger("isPickup");
                            Item1UIActive.sprite = customImage;
                            Item1UIActive.enabled = true;

                            withinRange = false;
                            PuzzleActive();
                            interactPrompt.SetActive(false);
                            collected = true;
                        }
                        //Item1UIActive.enabled = true;
                        break;

                    case "slot2Item":
                        if (Item2UIActive.enabled == false)
                        {
                            playerChar.charAnimator.SetTrigger("isPickup");
                            Item2UIActive.sprite = customImage;
                            Item2UIActive.enabled = true;

                            withinRange = false;
                            PuzzleActive();
                            interactPrompt.SetActive(false);
                            collected = true;
                        }
                        break;

                    case "slot3Item":
                        if (Item3UIActive.enabled == false)
                        {
                            playerChar.charAnimator.SetTrigger("isPickup");
                            Item3UIActive.sprite = customImage;
                            Item3UIActive.enabled = true;

                            withinRange = false;
                            PuzzleActive();
                            interactPrompt.SetActive(false);
                            collected = true;
                        }
                        break;
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            withinRange = true;
            interactPrompt.SetActive(true);
            playerChar = other.GetComponentInParent<Character_Movement>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            withinRange = false;
            interactPrompt.SetActive(false);
            playerChar = null;
        }
    }

    public virtual void PuzzleActive()
    {
        GetComponentInParent<Renderer>().enabled = false;
        searchingCollider.enabled = false;
        GetComponentInParent<Collider>().enabled = false;
    }

    public virtual void PuzzleInactive()
    {

    }
}