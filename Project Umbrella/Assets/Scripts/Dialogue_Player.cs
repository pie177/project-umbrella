﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogue_Player : MonoBehaviour {

    public AudioClip dialogueToPlay;
    Sound_System soundSystem;
    [Tooltip("Use to clarify between spoken lines and non speech, like breathing or sobbing.")]
    public bool isRepeatingDialgoue;
    [Tooltip("Use only if cue's existence depends on the presence of another object, like a key.")]
    public GameObject objectForDialogueExistence;
    [Tooltip("Use if cue should be triggered after an object is interacted with.")]
    public GameObject objectNeededToTriggerDialogue;


	// Use this for initialization
	void Start ()
    {
        soundSystem = GameObject.FindGameObjectWithTag("Sound_System").GetComponent<Sound_System>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(objectNeededToTriggerDialogue != null)
        {
            if(!objectNeededToTriggerDialogue.activeInHierarchy)
            {
                if (isRepeatingDialgoue)
                {
                    Debug.Log("Repeating speech played");
                    soundSystem.PlayBackgroundSpeech(dialogueToPlay);
                }
                else if (!isRepeatingDialgoue)
                {
                    if (!soundSystem.playerTalking.isPlaying)
                    {
                        Debug.Log("Speech played");
                        soundSystem.PlayPlayerSpeech(dialogueToPlay);
                        Destroy(this.gameObject);
                    }
                }
                objectNeededToTriggerDialogue = null;
            }
        }
		if(objectForDialogueExistence != null)
        {
            if(objectForDialogueExistence.activeInHierarchy)
            {
                this.gameObject.SetActive(true);
            }
            else
            {
                this.gameObject.SetActive(false);
            }
        }
	}

    public void OnTriggerEnter(Collider other)
    {
        if (soundSystem != null && other.CompareTag("Player") && objectNeededToTriggerDialogue == null)
        {
            if (isRepeatingDialgoue)
            {
                    Debug.Log("Repeating speech played");
                    soundSystem.PlayBackgroundSpeech(dialogueToPlay);
            }
            else if(!isRepeatingDialgoue)
            {
                if(!soundSystem.playerTalking.isPlaying)
                {
                    Debug.Log("Speech played");
                    soundSystem.PlayPlayerSpeech(dialogueToPlay);
                    Destroy(this.gameObject);
                }
                else
                {
                    Debug.Log("Player Talking already!");
                }
            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Player") && soundSystem != null && isRepeatingDialgoue)
        {
            Debug.Log("Speech Stopped");
            soundSystem.StopBackgroundSpeech();
            Destroy(this.gameObject);
        }
    }
}
