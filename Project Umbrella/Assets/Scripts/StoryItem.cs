﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum storyType
{
    textItem,
    lookItem
};

public class StoryItem : MonoBehaviour
{

    public GameObject lookUI;
    public GameObject textUI;

    public storyType storType = storyType.textItem;
    public GameObject interactionPrompt;
    public bool inRange = false;
    public GameObject lookItem;
    public GameObject textItem;
    public Animator charAnim;

    public string storyText;
    public Sprite textBackground;

    public GameObject objectOnDisplay;
    [Tooltip("What the player says while looking at an item")]
    public AudioClip lookSounds;

    private GameObject textBack;

    bool isReading;
    bool canInteract = true;

    GameObject temp;

    public Camera myCamera;
    bool isLooking = false;

    public float rotationSpeed;

    private void Awake()
    {
        textBack = GameObject.Find("TextBackground");
        myCamera = GetComponentInChildren<Camera>();
    }

    // Use this for initialization
    void Start()
    {
        if (storType == storyType.textItem)
        {
            lookItem.SetActive(false);
            textItem.SetActive(true);
        }
        else if (storType == storyType.lookItem)
        {
            textItem.SetActive(false);
            lookItem.SetActive(true);
        }
        lookUI.SetActive(false);
        textUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (inRange)
        {
            if (storType == storyType.textItem)
            {
                if (inRange && !isReading)
                {
                    if (Input.GetButtonDown("Interact") && canInteract)
                    {
                        canInteract = false;
                        runTextItem();
                        isReading = true;
                    }
                }
            }
            else if (storType == storyType.lookItem)
            {
                if (inRange && !isReading)
                {
                    if (Input.GetButtonDown("Interact") && canInteract)
                    {
                        canInteract = false;
                        runLookItem();
                        isReading = true;
                    }
                }
            }
            if (isReading)
            {
                if (Input.GetButtonDown("Interact") && canInteract)
                {
                    canInteract = false;
                    isLooking = false;
                    charAnim.GetComponentInParent<Character_Movement>().isPickup = false;
                    myCamera.gameObject.SetActive(false);
                    textUI.SetActive(false);
                    lookUI.SetActive(false);
                    isReading = false;
                    if (temp != null)
                    {
                        Destroy(temp);
                    }
                }
            }
            if (Input.GetButtonUp("Interact"))
            {
                canInteract = true;
            }
            if (isLooking)
            {
                //temp.transform.Rotate(new Vector3(-Input.GetAxis("Mouse Y"), -Input.GetAxis("Mouse X"), 0));
                //temp.transform.Rotate(Vector3.up, Input.GetAxis("Mouse X"));
                //temp.transform.Rotate(Vector3.right, Input.GetAxis("Mouse Y"));
                temp.transform.RotateAround(Vector3.up, Input.GetAxis("Mouse X") * Time.deltaTime * rotationSpeed);
                temp.transform.RotateAround(Vector3.right, -Input.GetAxis("Mouse Y") * Time.deltaTime * rotationSpeed);

                temp.transform.RotateAround(Vector3.up, -Input.GetAxis("ItemRotationX") * Time.deltaTime * rotationSpeed);
                temp.transform.RotateAround(Vector3.right, -Input.GetAxis("ItemRotationY") * Time.deltaTime * rotationSpeed);
            }
        }
    }

    void runLookItem()
    {
        myCamera.gameObject.SetActive(true);
        lookUI.SetActive(true);
        charAnim.GetComponentInParent<Character_Movement>().isPickup = true;
        temp = (GameObject)Instantiate(objectOnDisplay, myCamera.transform.position + myCamera.transform.forward * 10f, Quaternion.Euler(new Vector3(myCamera.transform.rotation.x, myCamera.transform.rotation.y - 180f, myCamera.transform.rotation.z)));
        isLooking = true;
    }

    void runTextItem()
    {
        textUI.SetActive(true);
        textBack.GetComponent<Image>().sprite = textBackground;
        textUI.GetComponentInChildren<Text>().text = storyText;
        charAnim.GetComponentInParent<Character_Movement>().isPickup = true;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            charAnim = other.GetComponentInChildren<Animator>();
            interactionPrompt.SetActive(true);
            inRange = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            interactionPrompt.SetActive(false);
            inRange = false;
        }
    }
}
