﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX_Example : MonoBehaviour {
	
	private Sound_System soundSystem;
	public AudioSource SFX;

	void Start()
	{
		soundSystem = GameObject.FindGameObjectWithTag ("Sound_System").GetComponent<Sound_System>();
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Player")) 
		{
			soundSystem.PlaySFX (SFX);
		}
	}
}
