﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class FadeStatic : MonoBehaviour {

    public VideoPlayer staticEffect;

    public float minStaticAlpha = 0.3f;

	// Use this for initialization
	void Start () {
        staticEffect = gameObject.GetComponent<VideoPlayer>();
        staticEffect.targetCameraAlpha = 2f;
	}
	
	// Update is called once per frame
	void Update () {
        staticEffect.targetCameraAlpha = Mathf.Max(staticEffect.targetCameraAlpha - (0.3f * Time.deltaTime), minStaticAlpha);
	}
}
