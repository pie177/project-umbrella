﻿Shader "Custom/StaticNoise" {
	Properties{
		_StaticXFactor("Static X Factor", float) = 1
		_StaticYFactor("Static Y Factor", float) = 1
		_Multiplier("Static Z Factor", float) = 1
		_Alpha("Alpha", Color) = (1,1,1,1)
	}
		SubShader
	{
		Tags { "RenderType" = "Transparent" }

		Pass
		{
			CGPROGRAM

			#pragma vertex vert_img

			#pragma fragment frag

			#include "UnityCG.cginc"

			float _StaticXFactor;
			float _StaticYFactor;
			float _Multiplier;
			float4 _Alpha;

			float noise(half2 uv)
			{
				return frac(sin(dot(uv, float2(_StaticXFactor, _StaticYFactor))) * _Multiplier);
			}

			fixed4 frag (v2f_img i) : SV_Target
			{
				fixed4 col = noise(i.uv);
				return col * _Alpha;
			}
			ENDCG
		}
	}
	//FallBack "Diffuse"
}
