﻿Shader "Test" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTexWall2("Wall XY Scene View",2D) = "surface"{}
		_MainTexWall("Wall ZY Scene View ",2D) = "surface"{}
		_Scale("Texture Scale" , Float) = 1.0
	}
		SubShader{
		Tags{"RenderType" = "Opaque"}
		LOD 200
		CGPROGRAM
#pragma surface surf Lambert

		sampler2D _MainTexWall2;
		sampler2D _MainTexWall;
		fixed4 _Color;
		float _Scale;

		struct Input {
			float3 worldNormal;
			float3 worldPos;
	};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			float2 UV;
			fixed4 c;
			if (abs(IN.worldNormal.x) > 0.5) {
				UV = IN.worldPos.yz;
				c = tex2D(_MainTexWall2, UV*_Scale);
			}
			else if (abs(IN.worldNormal.z) > 0.5) {
				UV = IN.worldPos.xy;
				c = tex2D(_MainTexWall, UV*_Scale);
			}
			else
			{
				UV = IN.worldPos.xz;
				c = tex2D(_MainTexWall, UV*_Scale);
			}
			o.Albedo = c.rgb *_Color;
		}
		ENDCG
	}

		Fallback "VertexLit"
}
