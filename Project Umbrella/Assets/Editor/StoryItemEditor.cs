﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

[CustomEditor(typeof(StoryItem))]
public class StoryItemEditor : Editor
{

    public override void OnInspectorGUI()
    {
        StoryItem myTarget = (StoryItem)target;

        myTarget.interactionPrompt = (GameObject)EditorGUILayout.ObjectField("Interaction Prompt", myTarget.interactionPrompt, typeof(GameObject), true);

        myTarget.lookItem = (GameObject)EditorGUILayout.ObjectField("My Look Model:", myTarget.lookItem, typeof(GameObject), true);
        myTarget.textItem = (GameObject)EditorGUILayout.ObjectField("My Text Model:", myTarget.textItem, typeof(GameObject), true);

        myTarget.lookUI = (GameObject)EditorGUILayout.ObjectField("My Look UI:", myTarget.lookUI, typeof(GameObject), true);
        myTarget.textUI = (GameObject)EditorGUILayout.ObjectField("My Text UI:", myTarget.textUI, typeof(GameObject), true);

        myTarget.storType = (storyType)EditorGUILayout.EnumPopup("Story Item Type", myTarget.storType);

        if (myTarget.storType == storyType.textItem)
        {
            EditorGUILayout.BeginVertical("box");
            EditorGUILayout.LabelField("Story text:");
            myTarget.storyText = EditorGUILayout.TextArea(myTarget.storyText);

            myTarget.textBackground = (Sprite)EditorGUILayout.ObjectField("Background to the text:", myTarget.textBackground, typeof(Sprite), false);

            EditorGUILayout.EndVertical();
        }
        if (myTarget.storType == storyType.lookItem)
        {
            EditorGUILayout.BeginVertical("box");

            myTarget.objectOnDisplay = (GameObject)EditorGUILayout.ObjectField("What the player will see:", myTarget.objectOnDisplay, typeof(GameObject), true);
            myTarget.lookSounds = (AudioClip)EditorGUILayout.ObjectField("What the player says:", myTarget.lookSounds, typeof(AudioClip), true);
            myTarget.rotationSpeed = EditorGUILayout.Slider("How fast OOD will rotate:", myTarget.rotationSpeed, 15f, 45f);

            EditorGUILayout.EndVertical();
        }
        if (GUI.changed)
        {
            EditorUtility.SetDirty(myTarget);
            EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
        }
    }
}
