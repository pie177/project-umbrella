﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

[CustomEditor(typeof(PuzzleController))]
public class PuzzleEditor : Editor {

	public override void OnInspectorGUI(){
		PuzzleController myTarget = (PuzzleController)target;

		myTarget.playerInv = (PlayerInventory)EditorGUILayout.ObjectField ("Player Inventory", myTarget.playerInv, typeof(PlayerInventory), true);

		myTarget.interactionPrompt = (GameObject)EditorGUILayout.ObjectField ("Interaction Prompt", myTarget.interactionPrompt, typeof(GameObject), true);

		myTarget.puzType = (puzzleType)EditorGUILayout.EnumPopup ("Puzzle Type", myTarget.puzType);

		if(myTarget.puzType == puzzleType.buttonPuzzle){
			EditorGUILayout.BeginVertical ("box");
            myTarget.doorToOpen = (GameObject)EditorGUILayout.ObjectField("Door To Open", myTarget.doorToOpen, typeof(GameObject), true);
            SerializedProperty myArray = serializedObject.FindProperty ("buttonArray");
			EditorGUI.BeginChangeCheck ();
			EditorGUILayout.PropertyField (myArray, new GUIContent("Button Order"), true);
			if (EditorGUI.EndChangeCheck ()) {
				serializedObject.ApplyModifiedProperties ();
			}

			EditorGUILayout.EndVertical ();
		}
		if (myTarget.puzType == puzzleType.itemPuzzle) {
			EditorGUILayout.BeginVertical ("box");

            myTarget.itType = (itemType)EditorGUILayout.EnumPopup("Item Type", myTarget.itType);

            EditorGUILayout.LabelField("Models:");
            EditorGUILayout.BeginVertical("box");
            myTarget.myCrowModel = (GameObject)EditorGUILayout.ObjectField("My Crowbar Model:", myTarget.myCrowModel, typeof(GameObject), true);
            myTarget.mykeyModel = (GameObject)EditorGUILayout.ObjectField("My Key Model:", myTarget.mykeyModel, typeof(GameObject), true);
            EditorGUILayout.EndVertical();

			EditorGUILayout.EndVertical ();
		}
        if (GUI.changed)
        {
            EditorUtility.SetDirty(myTarget);
            EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
        }
	}
}
