﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI_FAlling : MonoBehaviour {

    Sight_AI_Control sightParent;
    Sound_AI_Control soundParent;

    MonoBehaviour parent;

    NavMeshAgent agent;
    Animator anim;

	// Use this for initialization
	void Start ()
    {

        sightParent = transform.parent.GetComponent<Sight_AI_Control>();
        agent = sightParent.agent;
        anim = sightParent.anim;

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Hole"))
        {
            Debug.Log("Should be falling");
            sightParent.falling = true;
            transform.parent.GetComponent<NavMeshAgent>().enabled = false;
        }
    }
}
