﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderCollision : MonoBehaviour
{


    public bool IsValid;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Hole"))
        {
            IsValid = false;
            Destroy(this.gameObject);
        }
        else
        {
            IsValid = true;
            Destroy(this.gameObject, 3f);
        }
    }
}
