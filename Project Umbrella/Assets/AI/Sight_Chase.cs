﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Sight_Chase : StateMachineBehaviour {

    Sight_AI_Control control;
    NavMeshAgent agent;
    Animator fallingAnim;

	  //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log("I am now chasing");
        control = animator.GetComponent<Sight_AI_Control>();
        //fallingAnim = control.fallingAnim;
        agent = animator.transform.GetComponent<NavMeshAgent>();
        if(agent.enabled == true)
        {
            agent.isStopped = false;
            agent.SetDestination(control.player.transform.position);
            Debug.Log("Chasing Player");
        }
	}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(agent.enabled == true)
        {
            agent.SetDestination(control.player.transform.position);
        }
        else
        {
            agent.transform.Translate(Vector3.forward * Time.deltaTime * 5);

            agent.transform.Rotate(Vector3.right, 2);

        }
        
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        agent.speed = control.chaseSpeed;

        if(agent.enabled == true)
        {
            agent.isStopped = true;
        }
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
