﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Sound_Falling : MonoBehaviour {

    Sound_AI_Control soundParent;

    // Use this for initialization
    void Start()
    {
        soundParent = transform.parent.GetComponent<Sound_AI_Control>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Hole"))
        {
            Debug.Log("Should be falling");

                soundParent.agent.enabled = false;
                soundParent.transform.Translate(Vector3.forward * Time.deltaTime);
        }
    }
}