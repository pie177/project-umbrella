﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Sight_Wander : StateMachineBehaviour {

    Sight_AI_Control control;
    NavMeshAgent agent;

    float timer;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        control = animator.GetComponent<Sight_AI_Control>();
        agent = animator.transform.GetComponent<NavMeshAgent>();
        if (agent.enabled == true)
        {
            agent.isStopped = false;
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    if (agent.enabled == true)
    //    {
    //        timer += Time.deltaTime;

    //        if (timer >= control.wanderTime)
    //        {
                

    //            //GameObject tempWanderPoint = Instantiate(control.wanderPoint, wanderPos, Quaternion.identity);

    //            //if (tempWanderPoint.GetComponent<WanderCollision>().IsValid == true)
    //            {
    //                //agent.SetDestination(wanderPos);

    //                timer = 0;
    //            }
    //        }
    //    }
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
