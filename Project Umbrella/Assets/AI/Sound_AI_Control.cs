﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Sound_AI_Control : MonoBehaviour {

    public float chaseRange;
    //remember to change the agent speed
    public float chaseSpeed;
    public GameObject player;
    public NavMeshAgent agent;
    Animator anim;
    float distance;

    const string P_NOISEHEARD = "NoiseHeard";

    // Use this for initialization
    void Start ()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player_Sound"))
        {
            anim.SetBool(P_NOISEHEARD, true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player_Sound"))
        {
            anim.SetBool(P_NOISEHEARD, false);
        }
    }
}
