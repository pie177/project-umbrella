﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FindPosition : StateMachineBehaviour {

    Sight_AI_Control control;
    NavMeshAgent agent;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        animator.SetBool("OnNewLocation", false);

        //control = animator.GetComponent<Sight_AI_Control>();
        //agent = animator.GetComponent<NavMeshAgent>();

        //Vector3 randDirection = Random.insideUnitSphere * control.wanderDistance;

        //randDirection.x += animator.transform.position.x;
        //randDirection.z += animator.transform.position.z;

        //NavMeshHit navHit;

        //NavMesh.SamplePosition(randDirection, out navHit, control.wanderDistance, -1);

        //control.SetPoint(navHit.position);


    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //    control = animator.GetComponent<Sight_AI_Control>();
    //    agent = animator.GetComponent<NavMeshAgent>();

    //    Vector3 randDirection = Random.insideUnitSphere * control.wanderDistance;

    //    randDirection.x += animator.transform.position.x;
    //    randDirection.z += animator.transform.position.z;

    //    NavMeshHit navHit;

    //    if (NavMesh.SamplePosition(randDirection, out navHit, control.wanderDistance, -1))
    //    {
    //        animator.SetBool("OnNewLocation", false);
    //    }
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.SetBool("OnNewLocation", false);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
