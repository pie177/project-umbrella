﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class Sight_AI_Control : MonoBehaviour {

    public float chaseRange;
    public float chaseSpeed;
    public GameObject player;
    public float attackRange;
    [HideInInspector]
    public bool falling = false;

    public NavMeshAgent agent;
    //public Animator fallingAnim;

    //variables for the sound system
	public AudioSource enemyChase;
    public AudioSource enemyIdle;
	public Sound_System soundSystem;
    private bool idlePlaying;
    private bool chasePlaying;

    //variables for the animator
    public Animator anim;
    [HideInInspector]
    public float distance;

    //FSM paramaters
    const string P_PLAYERVISIBLE = "PlayerVisible";
    const string P_PLAYERINRANGE = "PlayerInRange";
    const string P_INATTACKRANGE = "InAttackRange";




	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animator>();

        agent = GetComponent<NavMeshAgent>();

        agent.speed = chaseSpeed;

		//soundSystem = GameObject.FindGameObjectWithTag ("Sound_System").GetComponent<Sound_System> ();

	}
	
	// Update is called once per frame
	void Update ()
    {
        PlayerInRange();

        //adds a falling enemy to the enemy count
        if(falling)
        {
            Destroy(this.gameObject, 3f);
            //Save_And_Load.instance.enemyCount++;
            //Vector3 fallingPos = fallingAnim.transform.position + transform.position;
            //transform.localPosition = Vector3.zero;
            //fallingAnim.transform.position = fallingPos;
            //transform.localPosition = Vector3.zero;
            //fallingAnim.SetBool("Falling", true);
            falling = false;
        }
	}

    /// <summary>
    /// This will calculate the range between the player and enemy and see if the player is close enough to chase
    /// </summary>
    void PlayerInRange()
    {
        distance = Vector3.Distance(transform.position, player.transform.position);
 
        //sees if the distance is less than or equal to the range the AI is allowed to chase
        if (distance <= chaseRange)
        {
            //if (soundSystem != null && chasePlaying == false)
            //{
            //    soundSystem.PlayEnemySounds(enemyChase);
            //    enemyIdle.Stop();
            //    chasePlaying = true;
            //    idlePlaying = false;
            //}
            //sets the paramater to true
            anim.SetBool(P_PLAYERINRANGE, true);
            Ray ray = new Ray(new Vector3(transform.position.x, transform.position.y + .5f, transform.position.z), player.transform.position - transform.position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, chaseRange))
            {
                if (hit.transform.CompareTag("Player_Sound"))
                {
                    anim.SetBool(P_PLAYERVISIBLE, true);

                    if (distance <= attackRange)
                    {
                        anim.SetBool(P_INATTACKRANGE, true);
                        enemyChase.Stop();
                        SceneManager.LoadScene("_GameOverScene");
                    }
                    else
                    {
                        anim.SetBool(P_INATTACKRANGE, false);
                        //if (enemyChase.isPlaying == false)
                        //{
                        //    soundSystem.PlayEnemySounds(enemyChase);
                        //}
                    }


                }
                else
                {
                    anim.SetBool(P_PLAYERVISIBLE, false);
                }
            }
        }
        else
        {
            anim.SetBool(P_PLAYERINRANGE, false);
            //if (soundSystem != null && idlePlaying == false)
            //{
            //    soundSystem.PlayEnemySounds(enemyIdle);
            //    enemyChase.Stop();
            //    chasePlaying = false;
            //    idlePlaying = true;
            //}
        }        
    }
}
